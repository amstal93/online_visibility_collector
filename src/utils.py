import yaml, json, os, logging

def log(func):
    def wrapper(*args, **kwargs):
        res = func(*args, **kwargs)
        logging.info('Running {} - {}'.format(func.__name__, args, kwargs))
        return res
    return wrapper

class ConfigFactory(object):
    def __init__(self, file):
        with open(file, 'r') as stream:
            try:
                d = yaml.safe_load(stream)
                self.ga_metrics      = d["google_analytics_metrics"]
                self.ga_dimensions   = d["google_analytics_dimensions"]
                self.gsc_metrics     = d["google_search_metrics"]
                self.gsc_dimensions  = d["google_search_dimensions"]
                
            except yaml.YAMLError as exc:
                raise exc
            # TODO: find a way to do this without volumes (env variables would be great)
            KEY_FILE_LOCATION = '/g_service_account.json'
            self.ga_key_conf = json.loads(open(KEY_FILE_LOCATION).read())

            self.ga_view_id = os.environ.get('GA_VIEW_ID', None)
            self.gs_url  = os.environ.get('GS_URL', None)
            try: 
              self.db_host = os.environ['DB_HOST']
              self.db_port = os.environ['DB_PORT']
              self.db_user = os.environ['INFLUXDB_USER']
              self.db_password = os.environ['INFLUXDB_USER_PASSWORD']
              self.db_name = os.environ['INFLUXDB_DB']
            except KeyError as e: 
              print("Missing ENV VARIABLE: {}".format(e))
              raise e 