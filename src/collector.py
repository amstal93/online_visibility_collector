import requests, ast, time, argparse, httplib2, inspect, schedule, logging, os
from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials
from abc import abstractmethod
from influxdb import InfluxDBClient
from utils import ConfigFactory, log
from datetime import date, timedelta, datetime
import oauth2client

class Batch(object): 
  '''structure data obtained through API. Make sure of its integrity with config file'''
  def __init__(self, **kwargs): 
    self.date             = kwargs.get('date')
    self.config           = kwargs.get('config')
    self.raw_data         = kwargs.get('raw_data',{})
    self.origin           = kwargs.get('origin', "unknown")
    try: 
      self.dimensions_names = self._get_dimensions_names()
      self.metrics_names    = self._get_metrics_names()
      self.rows             = self._get_rows()
    except: 
      logging.warning("Wrong data format from {}".format(self.origin))

  def _get_metrics_names(self): 
    if self.origin == "google_analytics":
      return [x["name"] for x in self.raw_data["columnHeader"]["metricHeader"]["metricHeaderEntries"]]
    elif self.origin == "google_search": 
      return sorted(self.config.gsc_metrics)

  def _get_rows(self): 
    if self.origin == "google_analytics": 
      return ({"dimensions": row["dimensions"], 
        "values":[ast.literal_eval(x) for x in row["metrics"][0]["values"]]} for row in self.raw_data["data"]["rows"])

    elif self.origin == "google_search": 
      return ({"dimensions": row["keys"] , "values": [x[1] for x in sorted(row.items()) if x[0] != "keys"]} for row in self.raw_data["rows"])
      
  def _get_dimensions_names(self):
    if self.origin == "google_analytics":
      return [x for x in self.raw_data["columnHeader"]["dimensions"]]
    elif self.origin == "google_search":
      return self.config.gsc_dimensions


class GoogleExtractor(object):
    """Google analytics data batch"""
    SCOPES = ['https://www.googleapis.com/auth/analytics.readonly', 'https://www.googleapis.com/auth/webmasters.readonly']
    def __init__(self, config):
        super(GoogleExtractor, self).__init__()
        self.credentials = ServiceAccountCredentials.from_json_keyfile_dict(config.ga_key_conf, type(self).SCOPES)
        self.config      = config
        self.end_date    = (date.today()-timedelta(days=1))
        self.start_date  = (date.today()-timedelta(days=2))

    @log
    def _get_google_search_daily(self): 
      analytics = build('webmasters', 'v3', credentials=self.credentials)

      r = {
        'startDate' : (self.start_date-timedelta(days=1)).strftime("%Y-%m-%d"),
        'endDate'   : (self.end_date-timedelta(days=1)).strftime("%Y-%m-%d") , 
        'dimensions': self.config.gsc_dimensions , 
        'searchType': 'web',
        'rowLimit'  : 10
      }
      
      return analytics.searchanalytics().query(siteUrl=self.config.gs_url, body=r).execute()

    @log
    def _get_google_analytics_daily(self):
      """fetch data from google analytics API v4 and EXTRACT relevant data ATTENTION: only returns one report """
      analytics = build('analyticsreporting', 'v4', credentials=self.credentials)

      return analytics.reports().batchGet(
          body={
            'reportRequests': [
            {
              'viewId': self.config.ga_view_id,
              'dateRanges': [{'startDate': self.start_date.strftime("%Y-%m-%d"), 'endDate': self.end_date.strftime("%Y-%m-%d")}],
              'metrics': [{'expression': x} for x in self.config.ga_metrics ],
              'dimensions': [{'name': x} for x in self.config.ga_dimensions]
            }]
          }
      ).execute()["reports"][0]

    @abstractmethod
    def extract(self, origin): 
      return Batch(raw_data = getattr(self, '_get_{}_daily'.format(origin))(), origin=origin, config=self.config, date=self.end_date)

class DataFormater(object): 
  def __init__(self, batch: Batch): 
    self.batch = batch

  @abstractmethod
  def __rshift__(self, db_method): 
    if type(db_method.__self__) == InfluxDBClient: 

      point_time = datetime.combine(self.batch.date, datetime.min.time()) 

      db_method( ({"measurement": self.batch.origin,
        "time" : point_time, 
        "tags": dict(zip(self.batch.dimensions_names, row["dimensions"])),
        "fields": dict(zip(self.batch.metrics_names, row["values"]))} for row in self.batch.rows ))

def main():
    logging.basicConfig(level=logging.INFO, format="%(asctime)s;%(levelname)s;%(message)s")
    logging.getLogger('googleapiclient.discovery_cache').setLevel(logging.ERROR)

    config = ConfigFactory("config.yml")
    db_client = InfluxDBClient(config.db_host, config.db_port, config.db_password, config.db_user, config.db_name)

    if 'GA_VIEW_ID' in os.environ:
      logging.info("Starting Google Analytics ingestion")
      batch = GoogleExtractor(config).extract("google_analytics")
      DataFormater(batch)>>db_client.write_points
        
    if 'GS_URL' in os.environ:
      logging.info("Starting Google Search ingestion")
      batch = GoogleExtractor(config).extract("google_search")
      DataFormater(batch)>>db_client.write_points
  
if __name__ == '__main__':

  parser = argparse.ArgumentParser()
  parser.add_argument("mode")
  args = parser.parse_args()

  if args.mode == "deamon": 
    schedule.every().day.at("06:30").do(main)
    while 1:
      schedule.run_pending()
      time.sleep(1)

  elif args.mode == "now":
    main() 
